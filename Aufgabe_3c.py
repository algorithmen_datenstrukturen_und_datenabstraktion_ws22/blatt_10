def rabin_karp(s, t):
    """Implementierung des Rabin-Karp-Algorithmus zur Suche der
    Zeichenkette(n) t in s.

    Parameters
    ----------
    t: str/list(str)
        Das Suchmuster oder eine Liste von Suchmustern
    """

    if type(t) == str:
        t = [t]
